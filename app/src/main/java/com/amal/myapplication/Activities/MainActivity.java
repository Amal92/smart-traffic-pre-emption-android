package com.amal.myapplication.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.amal.myapplication.Interfaces.AsyncTaskCompleteListener;
import com.amal.myapplication.Networking.HttpRequester;
import com.amal.myapplication.R;
import com.amal.myapplication.Utils.Commonutils;
import com.amal.myapplication.Utils.Const;
import com.amal.myapplication.Utils.EndPoint;
import com.amal.myapplication.Utils.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements AsyncTaskCompleteListener {

    private Button signIn;
    private EditText email, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (new PreferenceHelper(this).getUserId() != null){
            Intent intent = new Intent(this, Welcome.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }

        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        signIn = (Button) findViewById(R.id.signin);
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // startActivity(new Intent(MainActivity.this,Welcome.class));
                if (checkForValid(email.getText().toString(), password.getText().toString())) {
                    sendToServer();
                }
            }
        });
    }

    private void sendToServer() {
        HashMap<String,String> map = new HashMap<>();
        map.put(Const.URL,EndPoint.ServiceType.loginUrl);
        map.put(Const.Params.EMAIL,email.getText().toString());
        map.put(Const.Params.PASSWORD,password.getText().toString());

        new HttpRequester(this,Const.POST,map, EndPoint.ServiceCode.LOGIN,this);
    }


    private boolean checkForValid(String email, String password) {
        if (email.isEmpty()) {
            Commonutils.showtoast("Enter a valid email", this);
            return false;
        } else {
            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                Commonutils.showtoast("Enter a valid email", this);
                return false;
            }
        }
        if (password.isEmpty()) {
            Commonutils.showtoast("Enter a valid password", this);
            return false;
        }
        return true;
    }


    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        switch (serviceCode){
            case EndPoint.ServiceCode.LOGIN:
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.optString("status");
                    if (status.equals("1")){
                        Commonutils.showtoast(jsonObject.optString("message"),this);
                        JSONObject userObj = jsonObject.optJSONObject("user");
                        new PreferenceHelper(this).putUserId(userObj.optString("_id"));

                        Intent intent = new Intent(this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }else if (status.equals("0")){
                        Commonutils.showtoast(jsonObject.optString("message"),this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
