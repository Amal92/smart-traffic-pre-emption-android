package com.amal.myapplication.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.amal.myapplication.R;
import com.amal.myapplication.Utils.PreferenceHelper;
import com.amal.myapplication.mqtt.MqttActions;
import com.amal.myapplication.mqtt.MqttConstants;
import com.amal.myapplication.mqtt.MqttServerDetails;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class Welcome extends AppCompatActivity {

    String topic = "vivek/amal/iot/latlon";
    String content = "Message from MqttPublishSample";
    int qos = 2;
    String broker = "tcp://test.mosquitto.org:1883";
    String clientId = "JavaSample";
    MemoryPersistence persistence = new MemoryPersistence();
    private Button start;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        connectAndSubscibe();
        start = (Button) findViewById(R.id.start);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Welcome.this, Trip.class));
            }
        });


        /*try {
            MqttClient sampleClient = new MqttClient(broker, clientId, persistence);
            sampleClient.setCallback(new exampleCallBack());
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            System.out.println("Connecting to broker: "+broker);
            sampleClient.connect(connOpts);
            System.out.println("Connected");
            System.out.println("Publishing message: "+content);
            MqttMessage message = new MqttMessage(content.getBytes());
            message.setQos(qos);
            sampleClient.publish(topic, message);
        } catch (MqttException e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.logOut:
                new PreferenceHelper(this).Logout();
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void connectAndSubscibe() {
        Bundle dataBundle = new Bundle();
        dataBundle.putString(MqttConstants.server, MqttServerDetails.server);
        dataBundle.putString(MqttConstants.port, MqttServerDetails.port);
//        dataBundle.putString(MqttConstants.clientId, MqttServerDetails.deviceClientId);
        dataBundle.putString(MqttConstants.clientId, MqttServerDetails.getClientId());
        dataBundle.putInt(MqttConstants.action, MqttConstants.connect);
        dataBundle.putBoolean(MqttConstants.cleanSession, MqttServerDetails.cleanSession);
        dataBundle.putString(MqttConstants.message,
                MqttServerDetails.message);
        dataBundle.putString(MqttConstants.topic, MqttServerDetails.topic);
        dataBundle.putInt(MqttConstants.qos, MqttConstants.defaultQos);
        dataBundle.putBoolean(MqttConstants.retained,
                MqttConstants.defaultRetained);

        dataBundle.putString(MqttConstants.username,
                MqttServerDetails.deviceUsername);
        dataBundle.putString(MqttConstants.password,
                "AjKB!EYFg4p&G@6mn7");

        dataBundle.putInt(MqttConstants.timeout,
                MqttConstants.defaultTimeOut);
        dataBundle.putInt(MqttConstants.keepalive,
                MqttConstants.defaultKeepAlive);
        dataBundle.putBoolean(MqttConstants.ssl,
                MqttConstants.defaultSsl);

        MqttActions.getInstance().connectAction(this, dataBundle);
    }

    public class exampleCallBack implements MqttCallback {
        public void connectionLost(Throwable cause) {
            // Log.d(getClass().getCanonicalName(), "MQTT Server connection lost" + e.getReasonCode());
        }

        public void messageArrived(String topic, MqttMessage message) {
            Log.d(getClass().getCanonicalName(), "Message arrived:" + topic + ":" + message.toString());
        }

        public void deliveryComplete(IMqttDeliveryToken token) {
            Log.d(getClass().getCanonicalName(), "Delivery complete");
        }
    }

}
