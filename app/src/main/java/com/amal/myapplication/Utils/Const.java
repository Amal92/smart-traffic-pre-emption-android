package com.amal.myapplication.Utils;


/**
 * Created by user on 6/29/2015.
 */
public class Const {

    //General
    public static final String URL = "url";
    //for GET/POST method
    public static int GET = 0;
    public static int POST = 1;

    public class Params {
        public static final String PICTURE = "picture";
        public static final String EMAIL = "email";
        public static final String PASSWORD = "password";
        public static final String USER_ID = "id";
        public static final String SESSION_TOKEN = "token";
    }

}