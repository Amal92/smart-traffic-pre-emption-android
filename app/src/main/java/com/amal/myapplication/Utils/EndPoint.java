package com.amal.myapplication.Utils;

/**
 * Created by amal on 16/01/16.
 */
public class EndPoint {

    public class ServiceType {
        private static final String baseUrl = "http://socket-red.mybluemix.net/";
        public static final String loginUrl = baseUrl + "get_ambulance";
    }

    public class ServiceCode {
        public static final int LOGIN = 1;
    }

}
