package com.amal.myapplication.Utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by amal on 16/01/16.
 */
public class PreferenceHelper {
    public static final String PREF_NAME = "smart_traffic_pref";
    private SharedPreferences app_prefs;
    private Context context;

    public PreferenceHelper(Context context) {
        app_prefs = context.getSharedPreferences(PREF_NAME,
                Context.MODE_PRIVATE);
        this.context = context;
    }

    public void putUserId(String userId) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(Const.Params.USER_ID, userId);
        edit.apply();
    }

    public String getUserId() {
        return app_prefs.getString(Const.Params.USER_ID, null);
    }

    public void putSessionToken(String sessionToken) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(Const.Params.SESSION_TOKEN, sessionToken);
        edit.apply();
    }

    public String getSessionToken() {
        return app_prefs.getString(Const.Params.SESSION_TOKEN, null);
    }

    public void Logout() {
        putUserId(null);
        putSessionToken(null);
    }
}
