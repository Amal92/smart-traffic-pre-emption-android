package com.amal.myapplication.mqtt;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created by arjun on 11/19/15.
 */
public class MqttActions {
    public static MqttActions getInstance(){
        return new MqttActions();
    }
    private ChangeListener changeListener = new ChangeListener();
    /**
     * Process data from the connect action
     *
     * @param data the {@link Bundle}
     */
    public void connectAction(Context context, Bundle data) {
        MqttConnectOptions conOpt = new MqttConnectOptions();
    /*
     * Mutal Auth connections could do something like this
     *
     *
     * SSLContext context = SSLContext.getDefault();
     * context.init({new CustomX509KeyManager()},null,null); //where CustomX509KeyManager proxies calls to keychain api
     * SSLSocketFactory factory = context.getSSLSocketFactory();
     *
     * MqttConnectOptions options = new MqttConnectOptions();
     * options.setSocketFactory(factory);
     *
     * client.connect(options);
     *
     */

        // The basic client information
        String server = (String) data.get(MqttConstants.server);
        String clientId = (String) data.get(MqttConstants.clientId);
        int port = Integer.parseInt((String) data.get(MqttConstants.port));
        boolean cleanSession = (Boolean) data.get(MqttConstants.cleanSession);

        boolean ssl = (Boolean) data.get(MqttConstants.ssl);
        String ssl_key = (String) data.get(MqttConstants.ssl_key);
        String uri = null;
        if (ssl) {
            Log.e("SSLConnection", "Doing an SSL Connect");
            uri = "ssl://";

        }
        else {
            uri = "tcp://";
        }

        uri = uri + server + ":" + port;

        MqttAndroidClient client;
        client = MqttConnection.createClient(context, uri, clientId);

        if (ssl){
            try {
                if(ssl_key != null && !ssl_key.equalsIgnoreCase(""))
                {
                    FileInputStream key = new FileInputStream(ssl_key);
                    conOpt.setSocketFactory(client.getSSLSocketFactory(key,
                            "mqtttest"));
                }

            } catch (MqttSecurityException e) {
                Log.e(this.getClass().getCanonicalName(),
                        "MqttException Occured: ", e);
            } catch (FileNotFoundException e) {
                Log.e(this.getClass().getCanonicalName(),
                        "MqttException Occured: SSL Key file not found", e);
            }
        }

        // create a client handle
        String clientHandle = uri + clientId;

        // last will message
        String message = (String) data.get(MqttConstants.message);
        String topic = (String) data.get(MqttConstants.topic);
        Integer qos = (Integer) data.get(MqttConstants.qos);
        Boolean retained = (Boolean) data.get(MqttConstants.retained);

        // mqttConnection options

        String username = (String) data.get(MqttConstants.username);

        String password = (String) data.get(MqttConstants.password);

        int timeout = (Integer) data.get(MqttConstants.timeout);
        int keepalive = (Integer) data.get(MqttConstants.keepalive);

        MqttConnection mqttConnection = MqttConnection.createConnectionInstance(clientHandle, clientId, server, port,
                context, client, ssl);

        mqttConnection.registerChangeListener(changeListener);
        // connect client

        String[] actionArgs = new String[1];
        actionArgs[0] = clientId;
        mqttConnection.changeConnectionStatus(MqttConnection.ConnectionStatus.CONNECTING);

        conOpt.setCleanSession(cleanSession);
        conOpt.setConnectionTimeout(timeout);
        conOpt.setKeepAliveInterval(keepalive);
        if (!username.equals(MqttConstants.empty)) {
          //  conOpt.setUserName(username);
        }
        if (!password.equals(MqttConstants.empty)) {
          //  conOpt.setPassword(password.toCharArray());
        }

        final ActionListener callback = ActionListener.getActionListenerInstance(context,
                ActionListener.Action.CONNECT);

        boolean doConnect = true;

        if ((!message.equals(MqttConstants.empty))
                || (!topic.equals(MqttConstants.empty))) {
            // need to make a message since last will is set
            try {
                conOpt.setWill(topic, message.getBytes(), qos,
                        retained);
            }
            catch (Exception e) {
                Log.e(this.getClass().getCanonicalName(), "Exception Occured", e);
                doConnect = false;
                callback.onFailure(null, e);
            }
        }
        client.setCallback(new MqttCallbackHandler(context, clientHandle));


        //set traceCallback
        client.setTraceCallback(new MqttTraceCallback());

        mqttConnection.addConnectionOptions(conOpt);
        if (doConnect) {
            try {
                client.connect(conOpt, null, callback);
            }
            catch (MqttException e) {
                Log.e(this.getClass().getCanonicalName(),
                        "MqttException Occured", e);
            }
        }

    }


    /**
     * Subscribe to a topic that the user has specified
     */
    public void subscribeToCommand(Context context, String topic, String message)
    {
        topic = "iot-2/cmd/" + topic + "/fmt/json";
        int qos = MqttConstants.defaultQos;

        try {
            String[] topics = new String[1];
            topics[0] = topic;
            MqttConnection.getConnetion().getClient()
                    .subscribe(topic, qos, null, ActionListener.getActionListenerInstance(context, ActionListener.Action.SUBSCRIBE));
        }
        catch (MqttSecurityException e) {
            Log.e(this.getClass().getCanonicalName(), "Failed to subscribe to" + topic + " the client with the handle", e);
        }
        catch (MqttException e) {
            Log.e(this.getClass().getCanonicalName(), "Failed to subscribe to" + topic + " the client with the handle", e);
        }
    }

    /**
     * Publish the message the user has specified
     */
    public void publish(Context context, String smartCommunicatorId, String topic, String message)
    {

        topic = "iot-2/type/smart-communicator/id/" + smartCommunicatorId + "/cmd/" + topic + "/fmt/json";
//        topic = "iot-2/evt/" + topic + "/fmt/json";
        int qos = MqttConstants.defaultQos;

        boolean retained = MqttConstants.defaultRetained;
        try {
            MqttConnection.getConnetion().getClient()
                    .publish(topic, message.getBytes(), qos, retained, null, ActionListener.getActionListenerInstance(context, ActionListener.Action.PUBLISH));
        } catch (MqttException e) {
            Log.e(this.getClass().getCanonicalName(), "Failed to publish a messged from the client with the handle " + message, e);
        }

    }

    public void publishEvent(Context context, String topic, String message)
    {
       // topic = "iot-2/evt/" + topic + "/fmt/json";
        int qos = MqttConstants.defaultQos;
        boolean retained = MqttConstants.defaultRetained;
        try {
            MqttConnection.getConnetion().getClient()
                    .publish(topic, message.getBytes(), qos, retained, null, ActionListener.getActionListenerInstance(context, ActionListener.Action.PUBLISH));
        } catch (MqttException e) {
            Log.e(this.getClass().getCanonicalName(), "Failed to publish a message from the client with the handle " + message, e);
        }

    }

    public void disconnect(Context context) {
        MqttConnection c = MqttConnection.getConnetion();
        //if the client is not connected, process the disconnect
        if (!c.isConnected()) {
            return;
        }

        try {
            c.getClient().disconnect(null, ActionListener.getActionListenerInstance(context, ActionListener.Action.DISCONNECT));
        }
        catch (MqttException e) {
            Log.e(this.getClass().getCanonicalName(), "Failed to disconnect the client with the handle ", e);
        }

    }




    private class ChangeListener implements PropertyChangeListener {

        /**
         * @see PropertyChangeListener#propertyChange(PropertyChangeEvent)
         */
        @Override
        public void propertyChange(PropertyChangeEvent event) {

            if (!event.getPropertyName().equals(MqttConstants.ConnectionStatusProperty)) {
            }
        }

    }
}
