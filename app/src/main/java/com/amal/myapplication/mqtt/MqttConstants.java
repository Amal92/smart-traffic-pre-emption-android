/*******************************************************************************
 * Copyright (c) 1999, 2014 IBM Corp.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution. 
 *
 * The Eclipse Public License is available at 
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at 
 *   http://www.eclipse.org/org/documents/edl-v10.php.
 */
package com.amal.myapplication.mqtt;

import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * This Class provides constants used for returning results from an activity 
 *
 */
public class MqttConstants {

  /** Application TAG for logs where class name is not used*/
  public static final String TAG = "MQTT Android";

  /*Default values **/

  /** Default QOS value*/
  public static final int defaultQos = 2;
  /** Default timeout*/
  public static final int defaultTimeOut = 1000;
  /** Default keep alive value*/
  public static final int defaultKeepAlive = 10;
  /** Default SSL enabled flag*/
  public static final boolean defaultSsl = false;
  /** Default message retained flag */
  public static final boolean defaultRetained = true;
  /** Default last will message*/
  public static final MqttMessage defaultLastWill = null;
  /** Default port*/
  public static final int defaultPort = 1883;

  /** Connect Request Code */
  public static final int connect = 0;
  /** Advanced Connect Request Code  **/
  public static final int advancedConnect = 1;
  /** Last will Request Code  **/
  public static final int lastWill = 2;
  /** Show History Request Code  **/
  public static final int showHistory = 3;

  /* Bundle Keys */

  /** Server Bundle Key **/
  public static final String server = "server";
  /** Port Bundle Key **/
  public static final String port = "port";
  /** ClientID Bundle Key **/
  public static final String clientId = "clientId";
  /** Topic Bundle Key **/
  public static final String topic = "topic";
  /** History Bundle Key **/
  public static final String history = "history";
  /** Message Bundle Key **/
  public static final String message = "message";
  /** Retained Flag Bundle Key **/
  public static final String retained = "retained";
  /** QOS Value Bundle Key **/
  public static final String qos = "qos";
  /** User name Bundle Key **/
  public static final String username = "username";
  /** Password Bundle Key **/
  public static final String password = "password";
  /** Keep Alive value Bundle Key **/
  public static final String keepalive = "keepalive";
  /** Timeout Bundle Key **/
  public static final String timeout = "timeout";
  /** SSL Enabled Flag Bundle Key **/
  public static final String ssl = "ssl";
  /** SSL Key File Bundle Key **/
  public static final String ssl_key = "ssl_key";
  /** MqttConnections Bundle Key **/
  public static final String connections = "connections";
  /** Clean Session Flag Bundle Key **/
  public static final String cleanSession = "cleanSession";
  /** Action Bundle Key **/
  public static final String action = "action";

  /* Property names */

  /** Property name for the history field in {@link MqttConnection} object for use with {@link java.beans.PropertyChangeEvent} **/
  public static final String historyProperty = "history";

  /** Property name for the connection status field in {@link MqttConnection} object for use with {@link java.beans.PropertyChangeEvent} **/
  public static final String ConnectionStatusProperty = "connectionStatus";

  /* Useful constants*/

  /** Space String Literal **/
  public static final String space = " ";
  /** Empty String for comparisons **/
  public static final String empty = new String();

  public static final String CONNECTION_STATUS_UPDATE = "CONNECTION_STATUS_UPDATE";
  public static final int CONNECTION_SUCCESS = 1;
  public static final int CONNECTION_FAILURE = 0;
  public static final String DEVICE_WIFI_CONFIG_TOPIC = "wifiConfig";
  public static final String DEVICE_FSBL_CONFIG_TOPIC = "blisterConfig";
  public static final String PUBLISH_STATUS_UPDATE = "PUBLISH_STATUS_UPDATE";
  public static final int PUBLISH_SUCCESS = 100;

  public static final int PUBLISH_FAILURE = 101;
  public static final String NEW_ACTIVITY_MESSAGE_RECEIVED = "com.hector.android.NEW_ACTIVITY_MESSAGE_RECEIVED";
  public static final String MEDICATION_TAKEN_TOPIC = "medicationActivity";
}
